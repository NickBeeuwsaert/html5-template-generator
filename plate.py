#!/usr/bin/env python

from xml.dom import minidom
import sys
import json
htmlDoc = open("template.html", "r");
config = open(sys.argv[1]);
configOptions = json.load(config);
if "scripts" not in configOptions:
    configOptions["scripts"] = [];
if "styles" not in configOptions:
    configOptions["styles"] = [];
if "meta" not in configOptions:
    configOptions["meta"] = {};
if "title" not in configOptions:
    configOptions["title"] = "THIS IS A TITLE YOU SHITFUCK";
config.close();
document = minidom.parse(htmlDoc)
htmlDoc.close();
document.getElementsByTagName("html")[0].setAttribute("lang", configOptions["lang"] if "lang" in configOptions else "en");
headSection = document.getElementsByTagName("head")[0]
bodySection = document.getElementsByTagName("body")[0]
headSection.appendChild(document.createComment(" START META TAGS "));
bodySection.appendChild(document.createComment(" INSERT YOUR MOTHER FUCKING CONTENT HERE "));
headSection.appendChild(document.createElement("title")).appendChild(document.createTextNode(configOptions["title"]));
for key, value in configOptions["meta"].items():
    metaTag = document.createElement("meta");
    if type(value) is list:
        value = ', '.join(value);
    metaTag.setAttribute(key, value)
    headSection.appendChild(metaTag);
headSection.appendChild(document.createComment(" END META TAGS "));
headSection.appendChild(document.createComment(" START SCRIPT TAGS "));
for script in configOptions["scripts"]:
    scriptTag = document.createElement("script");
    scriptTag.setAttribute("src", script);
    headSection.appendChild(scriptTag);
headSection.appendChild(document.createComment(" END SCRIPT TAGS "));
headSection.appendChild(document.createComment(" START STYLE TAGS "));
for style in configOptions["styles"]:
    styleTag = document.createElement("link");
    styleTag.setAttribute("type", "text/css");
    styleTag.setAttribute("href", style);
    styleTag.setAttribute("rel","stylesheet");
    headSection.appendChild(styleTag);
headSection.appendChild(document.createComment(" END STYLE TAGS "));
if "IEHacks" in configOptions:
    headSection.appendChild(document.createComment(" BEGIN IEHACKS, BECAUSE IE SUCKS "));
    scripts = "<script>\n\tdocument.createElement(\"" + '");\n\tdocument.createElement("'.join(configOptions["IEHacks"]) + "\");\n\t</script>\n\t";
    headSection.appendChild(document.createComment("[if IE]>"+scripts+"<![endif]"));
    headSection.appendChild(document.createComment(" END IEHACKS, BECAUSE IE SUCKS "));
print '\n'.join(document.toprettyxml().split('\n')[1:]);
